import { exec } from 'child_process';
import watch from 'watch';

export default class BuildWatchman {
	constructor(cwd, src, executor) {
		console.log('[WATCHMAN] Initializing');

		this.executor = executor;

		this.cwd = cwd;
		this.src = src;

		this.active = false;
		this.runner = void 0;

		this.initWatchman();
	}

	initWatchman() {
		const { Compiler } = this;

		watch.watchTree(this.src, () => {
			if(this.active === true) return;
			console.log('[WATCHMAN] Changes detected');
			this.runBuild();
		});

		this.runBuild();
	}

	runBuild() {
		if(this.active === true) return;
		this.active = true;

		console.log('[WATCHMAN] Rebuilding');

		this.Compiler = this.executor();

		const Compiler = this.Compiler;
		const { Config } = this;

		Compiler.dispatcher.when('NeueWebpack').then( () => {
			this.active = false;

			this.startRunner();
		});
	}

	startRunner() {
		this.stopRunner();

		this.runner = exec('npm start', { cwd: this.cwd }, () => {
			console.log('[WATCHMAN] Server Child Process Started...');
		});
		const { runner } = this;

		runner.stdout.on('data', (data) => process.stdout.write(data));
		runner.stderr.on('data', (data) => process.stdout.write(data));
		runner.on('exit', (code, signal) => console.log(`[WATCHMAN] Server stopped (${code || signal})`));
	}

	stopRunner() {
		if(this.runner === void 0) return;
		try {
			this.runner.kill();
		} catch(e1) {
			try {
				this.runner.kill(9);
			} catch(e2) {
				console.log(`[WATCHMAN] ERROR: Unable to kill server child process. It may not be running anymore.`);
				console.log(`[WATCHMAN] ERROR: Kill Attempt 1: ${e1}`);
				console.log(`[WATCHMAN] ERROR: Kill Attempt 2: ${e2}`);
			}
		} finally {
			this.runner = void 0;
		}
	}
};
